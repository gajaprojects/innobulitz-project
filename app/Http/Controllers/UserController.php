<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;
use Auth;
use Session;
use Crypt;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \App\User::where('active_status', 1)->get();
        $count =count($user);
        return view('user',compact('user','count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\User::find($id);
        $address = \App\Address::where('user_id', $id)->get();
        return view('edit_user',compact('user','id','address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user= \App\User::find($id);
        $user->mobile=$request->get('mobile');
        $user->gender=$request->get('gender');
        if($request->file('image')!='') {
            $image = $request->file('image');
            $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img');
            $image->move($destinationPath, $input['imagename']);
            $user->image = $input['imagename'];
        }
        $user->active_status=1;
        $user->save();

        $deleteAddress=\App\Address::where('user_id',$id)->delete();

        $addressCount = count($request->address);
        for ($i = 0; $i < $addressCount; $i++) {
            $i . $address= new \App\Address;
            $i . $address->user_id=$id;
            $i . $address->address=$request->get('address')[$i];
            $i . $address->city=$request->get('city')[$i];
            $i . $address->state=$request->get('state')[$i];
            $i . $address->pincode=$request->get('pincode')[$i];
            if($request->get('rowId')[$i]==$request->get('primary')) {
                $i . $address->primary_address=1;
            } else {
                $i . $address->primary_address=0;
            }
            $i . $address->save();
        }
        return redirect('user')->with('success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user= \App\User::find($id);
        $user->active_status=0;
        $user->save();
        return redirect('user')->with('error', 'User deleted successfully');
    }

    public function userLogin()
    {
        return view('login');
    }

    public function userCheckLogin(Request $request)
    {
        request()->validate([
            'email' => 'required',
            'password' => 'required'
        ]);
        $email = $request->get('email');
        $password = $request->get('password');
        $user = \App\User::where('email', '=', $email)->orWhere('name', '=', $email)->first();
        if (!$user) {
           return response()->json(['status'=>false, 'msg' => 'Login Fail, please check email or username']);
        }
        else {
            if (!Hash::check($password, $user->password)) {
                return response()->json(['status'=>false, 'msg' => 'Login Fail, please check password']);
            } else {
                session(['id' =>  $user->id]);
                session(['name' =>  $user->name]);
                session(['email' =>  $user->email]);
                return response()->json(['status'=>true, 'msg' => 'Logged in successfully']);
            }
        }
    }

    public function register()
    {
        return view('register');
    }

    public function storeUser(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:users',
            'password' => 'required|min:5',
            'email' => 'required|email|unique:users'
        ]);

        $user= new \App\User;
        $user->name=$request->get('name');
        $user->email=$request->get('email');
        $user->password=Hash::make($request->get('password'));
        $user->mobile=$request->get('mobile');
        $user->gender=$request->get('gender');
        if($request->file('image')!='') {
            $image = $request->file('image');
            $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img');
            $image->move($destinationPath, $input['imagename']);
            $user->image = $input['imagename'];
        }
        $user->active_status=1;
        $user->save();

        $userDetails = \App\User::where('active_status', 1)->where('email','=', $request->get('email'))->get();
        session(['id' =>  $userDetails[0]->id]);
        session(['name' =>  $userDetails[0]->name]);
        session(['email' =>  $userDetails[0]->email]);

        $addressCount = count($request->address);
        for ($i = 0; $i < $addressCount; $i++) {
            $i . $address= new \App\Address;
            $i . $address->user_id=$userDetails[0]->id;
            $i . $address->address=$request->get('address')[$i];
            $i . $address->city=$request->get('city')[$i];
            $i . $address->state=$request->get('state')[$i];
            $i . $address->pincode=$request->get('pincode')[$i];
            if($request->get('rowId')[$i]==$request->get('primary')) {
                $i . $address->primary_address=1;
            } else {
                $i . $address->primary_address=0;
            }
            $i . $address->save();
        }
        return redirect('user')->with('success', 'Logged in successfully');
    }

    public function sessionLogout(Request $request)
    {
        $request->session()->flush();
        return redirect('/userLogin')->with('success', 'Logged out successfully!');
    }
}
