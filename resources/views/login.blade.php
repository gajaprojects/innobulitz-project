@extends('layouts.app')
@section('content')
<div class="container">
    <style>
        .error{ color:red; }
    </style>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="d-none d-md-block">&nbsp;</div>
            @if (\Session::has('success'))
                <div class="alert alert-success  text-center">
                <p>{{ \Session::get('success') }}</p>
                </div><br />
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger  text-center">
                <p>{{ \Session::get('error') }}</p>
                </div><br />
            @endif
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form id="login_form" method="post" action="javascript:void(0)">
                        @csrf
                        <div class="alert alert-success d-none" id="msg_div">
                            <span id="res_message"></span>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail Address / Username') }}</label>
                            <div class="col-md-8">
                                <input id="email" type="text" maxlength="30" class="form-control" name="email" value="" required autocomplete="email" autofocus>
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label">{{ __('Password') }}</label>
                            <div class="col-md-8">
                                <input id="password" type="password" maxlength="15" class="form-control" name="password" required autocomplete="current-password">
                                <span class="text-danger">{{ $errors->first('password') }}</span>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 text-md-right">
                                <button type="submit" id="send_form" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('#send_form').click(function(e){
       e.preventDefault();
       $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    $('#send_form').html('Sending..');
       $.ajax({
          url: "{{ url('userCheckLogin')}}",
          method: 'post',
          data: $('#login_form').serialize(),
          success: function(result){
              $('#send_form').html('Submit');
              if(result.status){
                  window.location.replace("./user");
              }else{
                  alert(result.msg);
                  $('#res_message').html(result.msg);
                  $('#msg_div').removeClass('alert-success');
                  $('#msg_div').addClass('alert-danger');
                  $('#msg_div').show();
                  $('#res_message').show();
              }
              document.getElementById("login_form").reset();
              setTimeout(function(){
              $('#res_message').hide();
              $('#msg_div').hide();
              },1500);
        }});
    });
});
</script>
@endsection
