@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="d-none d-md-block">&nbsp;</div>
            @if (\Session::has('success'))
                <div class="alert alert-success  text-center">
                <p>{{ \Session::get('success') }}</p>
                </div><br />
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger  text-center">
                <p>{{ \Session::get('error') }}</p>
                </div><br />
            @endif
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" action="{{ action('UserController@storeUser') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label">{{ __('Name') }}<span class="text-danger"> *</span></label>
                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required >
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail Address') }}<span class="text-danger"> *</span></label>
                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label">{{ __('Password') }}<span class="text-danger"> *</span></label>
                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" maxlength="15" required>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label">{{ __('Mobile') }}</label>
                            <div class="col-md-8">
                                <input id="mobile" type="number" class="form-control" name="mobile" value="" maxlength="10" min="0" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==10) return false;">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="image" class="col-md-4 col-form-label">{{ __('Image') }}</label>
                            <div class="col-md-4">
                                <input type="file" name="image" id="image" onchange="imagePreview(this);" />
                            </div>
                            <div class="col-md-4" align="right">
                                <img id="image_preview" width="100px" height="60px" src="{{URL::to('/')}}/img/no-image.jpg" />
                            </div>
                            <script type="text/javascript">
                            function imagePreview(input) {
                                if (input.files && input.files[0]) {
                                    var reader = new FileReader();
                                    reader.onload = function (e) {
                                        $('#image_preview').attr('src', e.target.result);
                                    }
                                    reader.readAsDataURL(input.files[0]);
                                }
                            }
                            </script>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label">{{ __('Gender') }}</label>
                            <div class="col-md-2">
                                <input id="gender" type="radio" name="gender" value="Male"> Male
                            </div>
                            <div class="col-md-2">
                                <input id="gender" type="radio" name="gender" value="Female"> Female
                            </div>
                            <div class="col-md-2">
                                <input id="gender" type="radio" name="gender" value="Other"> Other
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label">{{ __('Address') }}</label>
                            <div class="col-md-1">
                                <button type="button" class="btn btn-outline-success" onclick="addMoreRows(this.form);"><i class="fas fa-plus"></i></button>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <textarea class="form-control" name="address[]" value="" placeholder="Address" maxlength="350" rows="3" cols="30"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input id="city" type="text" class="form-control" name="city[]" value="" placeholder="City" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <select class="form-control" name="state[]" id="state">
                                            <option value="TN">Tamilnadu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input id="pincode" type="number" class="form-control" name="pincode[]" placeholder="Pincode" value="" maxlength="6" min="0" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==6) return false;">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="hidden" id="rowId" name="rowId[]" value="1">
                                        <input id="primary" type="radio" name="primary" value="1"> Set as Primary
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="addedRows"></div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 text-md-right">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var rowCount = 100;
    function addMoreRows(frm) {
        rowCount ++;
        var recRow = '<div class="form-group row" id="row'+rowCount+'"><label for="password" class="col-md-4 col-form-label">&nbsp;</label><div class="col-md-1"><button type="button" class="btn btn-outline-danger" onclick="removeRow('+rowCount+');"><i class="fas fa-minus"></i></button></div><div class="col-md-7"><div class="form-group row"><div class="col-md-12"><textarea class="form-control" name="address[]" value="" placeholder="Address" maxlength="350" rows="3" cols="30"></textarea></div></div><div class="form-group row"><div class="col-md-12"><input id="city" type="text" class="form-control" name="city[]" value="" placeholder="City" ></div></div><div class="form-group row"><div class="col-md-12"><select class="form-control" name="state[]" id="state"><option value="TN">Tamilnadu</option></select></div></div><div class="form-group row"><div class="col-md-6"><input id="pincode" type="number" class="form-control" name="pincode[]" placeholder="Pincode" value="" maxlength="6" min="0" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==6) return false;"></div><div class="col-md-6"><input type="hidden" id="rowId" name="rowId[]" value="'+rowCount+'"><input id="primary" type="radio" name="primary" value="'+rowCount+'"> Set as Primary</div></div></div>';
        var addressCount = $('textarea[name="address[]"]').length;
		if(addressCount<5) {
            jQuery('#addedRows').append(recRow);
		} else {
			alert("Sorry, Maximum 5 addresses allowed.");
		}
    }
    function removeRow(removeNum) {
        if(removeNum == 1) {
            alert("Cannot Delete this Row");
            return false;
        } else {
            var didConfirm = confirm("Are you sure You want to delete");
            if (didConfirm == true) {
                var id = jQuery(this).attr('data-id');
                var targetDiv = jQuery(this).attr('targetDiv');
                jQuery('#row'+removeNum).remove();
                return true;
            } else {
                return false;
            }
        }
    }
    </script>
@endsection
