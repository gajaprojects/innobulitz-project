@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" action="{{action('UserController@update', $id)}}">
                        @csrf
                        <input name="_method" type="hidden" value="PATCH">

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label">{{ __('Name') }}<span class="text-danger"> *</span></label>
                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control" name="name" value="{{$user['name']}}" readonly >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail Address') }}<span class="text-danger"> *</span></label>
                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control" name="email" value="{{$user['email']}}" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label">{{ __('Mobile') }}</label>
                            <div class="col-md-8">
                                <input id="mobile" type="number" class="form-control" name="mobile" value="{{$user['mobile']}}" maxlength="10" min="0" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==10) return false;">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="image" class="col-md-4 col-form-label">{{ __('Image') }}</label>
                            <div class="col-md-4">
                                <input type="file" name="image" id="image" onchange="imagePreview(this);" />
                            </div>
                            <div class="col-md-4" align="right">
                                <img id="image_preview" width="100px" height="60px" src="{{URL::to('/')}}/img/{{$user['image']}}" />
                            </div>
                            <script type="text/javascript">
                            function imagePreview(input) {
                                if (input.files && input.files[0]) {
                                    var reader = new FileReader();
                                    reader.onload = function (e) {
                                        $('#image_preview').attr('src', e.target.result);
                                    }
                                    reader.readAsDataURL(input.files[0]);
                                }
                            }
                            </script>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label">{{ __('Gender') }}</label>
                            <div class="col-md-2">
                                <input id="gender" type="radio" name="gender" value="Male" {{$user['gender']=='Male' ? 'checked' : ''}}> Male
                            </div>
                            <div class="col-md-2">
                                <input id="gender" type="radio" name="gender" value="Female" {{$user['gender']=='Female' ? 'checked' : ''}}> Female
                            </div>
                            <div class="col-md-2">
                                <input id="gender" type="radio" name="gender" value="Other" {{$user['gender']=='Other' ? 'checked' : ''}}> Other
                            </div>
                        </div>
                        @foreach ($address as $value)
                            <div class="form-group row" id="row{{$value['id']}}">
                                <label for="password" class="col-md-4 col-form-label">@if($loop->index==0){{ __('Address') }}@endif</label>
                                <div class="col-md-1">
                                    @if($loop->index==0)
                                        <button type="button" class="btn btn-outline-success" onclick="addMoreRows(this.form);"><i class="fas fa-plus"></i></button>
                                    @else
                                        <button type="button" class="btn btn-outline-danger" onclick="removeRow({{$value['id']}});"><i class="fas fa-minus"></i></button>
                                    @endif
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <textarea class="form-control" name="address[]" value="" placeholder="Address" maxlength="350" rows="3" cols="30">{{$value['address']}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <input id="city" type="text" class="form-control" name="city[]" value="{{$value['city']}}" placeholder="City" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <select class="form-control" name="state[]" id="state">
                                                <option value="TN" {{$value['state']=='TN' ? 'selected' : ''}}>Tamilnadu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <input id="pincode" type="number" class="form-control" name="pincode[]" placeholder="Pincode" value="{{$value['pincode']}}" maxlength="6" min="0" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==6) return false;">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="hidden" id="rowId" name="rowId[]" value="{{$loop->index}}">
                                            <input id="primary" type="radio" name="primary" value="{{$loop->index}}" {{$value['primary_address']==1 ? 'checked' : ''}}> Set as Primary
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div id="addedRows"></div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 text-md-right">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var rowCount = 100;
    function addMoreRows(frm) {
        rowCount ++;
        var recRow = '<div class="form-group row" id="row'+rowCount+'"><label for="password" class="col-md-4 col-form-label">&nbsp;</label><div class="col-md-1"><button type="button" class="btn btn-outline-danger" onclick="removeRow('+rowCount+');"><i class="fas fa-minus"></i></button></div><div class="col-md-7"><div class="form-group row"><div class="col-md-12"><textarea class="form-control" name="address[]" value="" placeholder="Address" maxlength="350" rows="3" cols="30"></textarea></div></div><div class="form-group row"><div class="col-md-12"><input id="city" type="text" class="form-control" name="city[]" value="" placeholder="City" ></div></div><div class="form-group row"><div class="col-md-12"><select class="form-control" name="state[]" id="state"><option value="TN">Tamilnadu</option></select></div></div><div class="form-group row"><div class="col-md-6"><input id="pincode" type="number" class="form-control" name="pincode[]" placeholder="Pincode" value="" maxlength="6" min="0" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==6) return false;"></div><div class="col-md-6"><input type="hidden" id="rowId" name="rowId[]" value="'+rowCount+'"><input id="primary" type="radio" name="primary" value="'+rowCount+'"> Set as Primary</div></div></div>';
        var addressCount = $('textarea[name="address[]"]').length;
		if(addressCount<5) {
            jQuery('#addedRows').append(recRow);
		} else {
			alert("Sorry, Maximum 5 addresses allowed.");
		}
    }
    function removeRow(removeNum) {
        if(removeNum == 1) {
            alert("Cannot Delete this Row");
            return false;
        } else {
            var didConfirm = confirm("Are you sure You want to delete");
            if (didConfirm == true) {
                var id = jQuery(this).attr('data-id');
                var targetDiv = jQuery(this).attr('targetDiv');
                jQuery('#row'+removeNum).remove();
                return true;
            } else {
                return false;
            }
        }
    }
    </script>
@endsection
